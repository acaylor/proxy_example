# Proxy example

In `docker-compose.yml` there are two containers. The nginx container acts as a reverse proxy server for the echoserver. When both containers are running, only the nginx container should have a port forwarded to your local system.

```sh
docker compose up -d
```

Once container is up, check your localhost port 8080

[localhost:8080](http://127.0.0.1:8080/)